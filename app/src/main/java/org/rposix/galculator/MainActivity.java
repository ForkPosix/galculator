package org.rposix.galculator;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.SupportActivity;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends SupportActivity {
    //tanımlama
    TextView resultText;
    EditText editText;
    EditText editText1;
    //int myColor= 0xFF202833;
    int btnColor =0xFF4a7ca5;
    int arkaPlan = 0xFF173145;
    int yardimRengi = 0xff47ae84;
    int yaziRengi = Color.YELLOW;
    int sonucRengi = 0xff47ae84;
    int uyariRengi = Color.RED;
    String felcli= "Sayı Gir!";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //aktivite başladığında
        editText = findViewById(R.id.editText);
        editText1 = findViewById(R.id.editText2);
        resultText = findViewById(R.id.textView);
        //Renkler
        final RelativeLayout rl= (RelativeLayout) findViewById(R.id.rl);
        rl.setBackgroundColor(arkaPlan);
        EditText editText = findViewById(R.id.editText);
        EditText editText2 = findViewById(R.id.editText2);
        Button toplama = findViewById(R.id.toplama);
        Button eksi = findViewById(R.id.eksi);
        Button carpma = findViewById(R.id.carpma);
        Button bolme = findViewById(R.id.bolme);
        editText.setBackgroundColor(btnColor);
        editText2.setBackgroundColor(btnColor);
        editText.setTextColor(yaziRengi);
        editText2.setTextColor(yaziRengi);
        editText.setHint("Sayı giriniz...");
        editText2.setHint("Sayı giriniz...");
        editText.setHintTextColor(yardimRengi);
        editText2.setHintTextColor(yardimRengi);
        toplama.setBackgroundColor(btnColor);
        toplama.setTextColor(yaziRengi);
        eksi.setBackgroundColor(btnColor);
        eksi.setTextColor(yaziRengi);
        carpma.setBackgroundColor(btnColor);
        carpma.setTextColor(yaziRengi);
        bolme.setBackgroundColor(btnColor);
        bolme.setTextColor(yaziRengi);
        ActionBar ab = getActionBar();
        ab.setTitle(Html.fromHtml("<font color='#47ae84'>Galculator</font>"));
        ab.setBackgroundDrawable(new ColorDrawable(arkaPlan));
        ab.setLogo(R.drawable.baslik);
        resultText.setTextColor(yaziRengi);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(arkaPlan);
            window.setNavigationBarColor(arkaPlan);
        }

    }



    public void toplama (View view){

        if (editText.getText().toString().matches("") || editText1.getText().toString().matches("")){
            resultText.setTextColor(uyariRengi);
            resultText.setText(felcli);
        } else {
            double a = Double.parseDouble(editText.getText().toString());
            double b = Double.parseDouble(editText1.getText().toString());
            double resultDouble = a + b;
            resultText.setTextColor(sonucRengi);
            resultText.setText("Sonuç: " + resultDouble);
        }


    }

    public void eksi (View view){

        if (editText.getText().toString().matches("") || editText1.getText().toString().matches("")){
            resultText.setTextColor(uyariRengi);
            resultText.setText(felcli);
        } else {
            double a = Double.parseDouble(editText.getText().toString());
            double b = Double.parseDouble(editText1.getText().toString());
            double resultDouble = a - b;
            resultText.setTextColor(sonucRengi);
            resultText.setText("Sonuç: " + resultDouble);
        }

    }

    public void carpma (View view){

        if (editText.getText().toString().matches("") || editText1.getText().toString().matches("")){
            resultText.setTextColor(uyariRengi);
            resultText.setText(felcli);
        } else {
            double a = Double.parseDouble(editText.getText().toString());
            double b = Double.parseDouble(editText1.getText().toString());
            double resultDouble = a * b;
            resultText.setTextColor(sonucRengi);
            resultText.setText("Sonuç: " + resultDouble);
        }

    }
    public void bolme (View view){

        if (editText.getText().toString().matches("") || editText1.getText().toString().matches("")){
            resultText.setTextColor(uyariRengi);
            resultText.setText(felcli);
        } else {
            double a = Double.parseDouble(editText.getText().toString());
            double b = Double.parseDouble(editText1.getText().toString());
            double resultDouble = a / b;
            resultText.setTextColor(sonucRengi);
            resultText.setText("Sonuç: " + resultDouble);
        }

    }

}
